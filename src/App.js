import React, { useEffect, useState } from 'react';
import { 
  AppBar, Box, Toolbar, Typography, Button, Chip, Container, TextField, Divider
} from '@mui/material';

import { ethers } from 'ethers';
import { formatEther, parseUnits } from '@ethersproject/units';
import abi from './abi.json';

import { initializeConnector } from '@web3-react/core';
import { MetaMask } from '@web3-react/metamask';

const [metaMask, hooks] = initializeConnector((actions) => new MetaMask(actions))
const { useAccounts, useError, useIsActive, useProvider } = hooks
const contractChain = 55556
const contractAddress  = '0xe40A7693684Cec3b46A5ada713949929456454f2'

const getAddressTxt = (str, s = 6, e = 6) => {
  if (str) {
    return `${str.slice(0, s)}...${str.slice(str.length - e)}`;
  }
  return "";
};

function App() {
  const connector = metaMask
  const accounts = useAccounts()
  const error = useError()
  const isActive = useIsActive()
  const provider = useProvider()

  const [isLoading, setIsLoading] = useState(true)
  const [myBalance, setMyBalance] = useState('')
  const [reiValue, setReiValue] = useState(0)

  useEffect(() => {
    const getBalance = async () => {
      try {
        const signer = provider.getSigner()
        const smartContract = new ethers.Contract(contractAddress, abi, signer)
        const balance = await smartContract.balanceOf(accounts[0])
        console.log(formatEther(balance))
        setMyBalance(formatEther(balance))
      } catch (err) {
        console.log(err)
      }
    }

    if (isLoading) {
      void metaMask.connectEagerly()
      setIsLoading(false)
    }

    if (!isLoading && isActive) {
      getBalance()
    }
  }, [isLoading, isActive])

  const handleConnect = () => {
    connector.activate(contractChain)
  }

  const hanldeDisconnect = () => {
    connector.deactivate()
  }

  const handleBuyMTK = async () => {
    try {
      const signer = provider.getSigner()
      const smartContract = new ethers.Contract(contractAddress, abi, signer)
      const txHash = await smartContract.buy({
        from: accounts[0],
        value: parseUnits(reiValue, "ether")
      })
      smartContract.on("Buy", (from, to, tokens) => {
        console.log(from, to, tokens, txHash)
        setIsLoading(true)
      })
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <div>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              PTK
            </Typography>
            {isActive ?
              <>
                <Chip label={getAddressTxt(accounts[0])} variant="outlined" />
                <Button color="inherit" onClick={hanldeDisconnect}>Disconnect</Button>
              </>
            :
              <Button color="inherit" onClick={handleConnect}>Metamask</Button>
            }
          </Toolbar>
        </AppBar>
        { isActive && 
          <Container maxWidth="sm" sx={{ mt: 4 }}>
            <Typography variant="h6" gutterBottom component="div" align='center'>
              PTK
            </Typography>
            <TextField id="outlined-basic" label="My Address" sx={{ mt: 2 }}
              variant="outlined" value={accounts[0]} fullWidth
            />
            <TextField id="outlined-basic" label="My Balance" sx={{ mt: 2 }}
              variant="outlined" value={myBalance} fullWidth
            />
            <Typography variant="h6" gutterBottom component="div" align='center' sx={{ mt: 2 }}>
              Buy PTK (1 REI = 10 PTK)
            </Typography>
            <TextField id="outlined-basic" label="REI" sx={{ mt: 2 }}
              variant="outlined" fullWidth
              onChange={(event) => {setReiValue(event.target.value)}} 
            />
            <Button variant="contained" fullWidth sx={{ mt: 2 }} onClick={handleBuyMTK}>Buy</Button>
          </Container>
        }
      </Box>
    </div>
  );
}

export default App;